import 'dart:io';

int nClient =0;

void main(){
  ServerSocket.bind(InternetAddress.anyIPv4 , 3000).then(
    (server){
      server.listen((client){
        handleConnection(client);
      });
    }
  ); 
}

void handleConnection(Socket client){
  int n= ++nClient;
  print('client '+nClient.toString()+'connect from '+
  '${client.remoteAddress.address}:${client.remotePort}');
  client.listen((data){
    String str =String.fromCharCodes(data).trim().toUpperCase();
    print('[$n]: '+str);
    client.write(str);
  },
  onDone : () {client.close();} 
  );
}