import 'dart:io';

import 'package:flutter/material.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(
        title: 'Cient',
        ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var isConnect = false;
  var strConnection = 'disconnected';
  final myController = TextEditingController();
  var inputStr;
  int iText =0;
  int iLabel=0;
  String ipAddress;
  int port;
  List text = <String>[];
  List labelText = <String>['ipAddress','port','message'];
  

  void dispose(){
    myController.dispose();
    super.dispose();
  }

  Socket writer;

  void connect(){
    Socket.connect(InternetAddress(ipAddress), port).then((client){
      writer = client;
      aggiorna(writer);
    }).catchError((e){
      if(e is SocketException)
      print('ScocketException => $e');
      setState(() {
        isConnect=false;
      });
    });
  }

  void aggiorna(Socket server){
    writer.write('test\n');
    server.listen((data){
      String s = String.fromCharCodes(data).trim();
      setState(() {
        if(s.isNotEmpty){
          isConnect=true;
          strConnection='connected';
        }
      });
      print(strConnection);
      setState(() {
        text[iText]+='\n'+s;
        iText++;
      });
    },
    onDone : () {server.close();}
    );
  }

  void disconnect(){
    writer.close();
    setState(() {
      strConnection='disconnected';
      isConnect=false;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(widget.title),
      ),

      body: 
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            TextField(
              controller: myController,
              decoration: InputDecoration(labelText: labelText[iLabel]),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FloatingActionButton(
                  onPressed: (){
                    setState(() {
                      if(iLabel == 0){
                        ipAddress=myController.text;
                        myController.text='';
                        iLabel++;
                      }else if(iLabel == 1){
                        port=int.parse(myController.text);
                        myController.text='';
                        iLabel++;
                      }else{
                        inputStr=myController.text+'\n';
                        myController.text='';
                        text.add(inputStr);
                      }
                    });
                    writer.write(inputStr);
                  },
                  child: Icon(Icons.send),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 300,
                  width: 300,
                  child: ListView.separated(
                    separatorBuilder: (BuildContext context , int index) => const Divider(),
                    padding: const EdgeInsets.all(10),
                    itemCount: text.length,
                    itemBuilder: (BuildContext cotext , int index){
                      return Container(
                        height: 50,
                        child: Text('${text[index]}'),
                      );
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: Stack(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(width: 80,),
                  FloatingActionButton.extended(
                    onPressed: (){
                      setState(() {
                        ipAddress='';
                        port=0;
                        iLabel=0;
                      });
                      disconnect();
                    },
                    backgroundColor: Colors.yellow,
                    label: Text('changeIP'),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(width: 50,),
                  FloatingActionButton(
                    onPressed: (){
                      setState(() {
                        text.clear();
                        iText=0;
                      });
                    },
                    child :  Icon(Icons.delete_forever),
                    backgroundColor: Colors.red,
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(width: 80,),
                  FloatingActionButton.extended(
                    onPressed: (){ 
                      setState(() {
                        if(isConnect==false) {
                          connect();
                        }else disconnect();
                      });
                    },
                    icon : Icon(Icons.wifi),
                    backgroundColor: Colors.green,
                    label : Text('$strConnection'),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}