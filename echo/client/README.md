# Server:
per la parte server mi sono basato su un esempio riportato su github.

# Client:
il lato client invece è un applicazione flutter in linguaggio dart che
tramite socket e server socket si connette con il server echo sopracitato.
Il client invia un messaggio al server il quale lo rinvia ma in stampatello 
grazie al metodo toUpperCase().
La parte grafica dell'applicazione si basa su un sistema di riche e colonne,che 
permettono il posizionamento dei widget.
Nella parte superiore possiamo trovare un **Text Field** con un **TextEditingController**
il quale ci permette di scrivere con la tastiera.Subito sotto troviamo il 
**FloatingAcitionButton** di invio il quale in base alla variabile **iLabel** 
memorizza **ipAddress** se uguale a 0,**port** se 1 e **message** se 2.
Continuondo verso il basso troviamo una **ListView.separated** detro la quale vengono
visualizzati i messaggi e la relativa risposta dal server.
Nella bottomApp invece troviamo tre **FloatingActionButton** che permettono di connetere il 
client al server con la funzione  **connect**,il quale connette il Socket **writer** al server,
e richiama la funzione **aggiorna** che mette il sochet in ascolto con il medono **listen**,e qui
viene effetuato il controllo della connessione inviando un messaggio di prova e controllando che 
non sia vuoto,se non vuoto si portano la viabile **isConnect**(inzializzata a false) a true,
e cambia la stringa **strConnection**(inizializzata a disconnected) a connected;se ripremuto viene 
chiamata la funzione **disconnect** la quale chiude la connessione con il server.
Nella funzione agiorna inoltre viene modificato il contenito della listView, con l'aggiunta del 
messaggio arrivato alla lista **text**.
Alla sinistra troviamo il bottone per svuotare la listView.
In conclusione troviamo il bottone che ci permette di reimpostare l'indirizzo ip e la porta,
impostando le variabili corrispondenti a '' e 0, e chiudendo la comunicazione con il server.

# Documentazione:
API flutter

stack overflow
