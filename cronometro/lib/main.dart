import 'dart:async';

import 'package:flutter/material.dart';

void main() async => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cronometro',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Cronometro'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  

  var isRunning = false;
  var min = 0;
  var h = 0;
  var sec = 0;

  Future <void> start () async { 
    await Future.delayed (Duration (seconds: 0));
    if(isRunning) return; 
    isRunning=true;
    loop();
  }

  Future <void> stop () async { 
    await Future.delayed (Duration (seconds: 0));
    isRunning=false;
  }

  Future <void> reset () async { 
    await Future.delayed (Duration (seconds: 0)); 
    setState(() {
      sec=0;
      min=0;
      h=0;
    });
  }

  void updateTime () {
    setState((){
      if(isRunning) sec++;
      if(sec == 60) {
        min ++;
        sec=0;
      }
      if(min == 60) {
        h++;
        min=0;
      }  
    });
  }

  Future <void> loop() async {
    while(isRunning){
      await Future.delayed (Duration (seconds: 1));
      updateTime();
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         title: Text('Cronometro'),
      ),
      body: Center(
        child : Text(
          '$h.$min.$sec',
          style : TextStyle(fontSize: 30),
          ),
      ),
      bottomNavigationBar: BottomAppBar(
        color : Colors.blue,
        child: Container(height: 20.0),
      ),

      floatingActionButton: Stack(
        children: <Widget>[
          Padding(padding: EdgeInsets.only(left: 31,),
          child: 
            Align(
              alignment: Alignment.bottomLeft,
              child: FloatingActionButton(
                onPressed: () async {
                  await start();
                },
                backgroundColor: Colors.green,
                child: Icon(Icons.play_arrow),
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(left:31,),
          child:
            Align(
              alignment: Alignment.bottomCenter,
              child: FloatingActionButton(
                onPressed: () async{
                  await reset();
                },
                backgroundColor: Colors.blue,
                child: Icon(Icons.refresh),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: FloatingActionButton(
              onPressed: () async{
              await stop();
              },
              backgroundColor: Colors.red,
              child: Icon(Icons.stop),
            ),
          ),
        ],
      ),
    );
  }
}