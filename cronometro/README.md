# cronometro

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


**Spiegazione:**
L'applicazione scritta in linuaggio dart, riporta il funzionamento di un
cronometro con precisione ai secondi.
Per funzionare l'applicazione fa uso : degli stream flutter utilizzati nelle 4 principali 
funzioni start() stop() reset() e loop(),quest'ultima viene richiamata nella funzione start(),
e finchè la variabile isRunnig è true,richiama la funzione updateTime, la quale va a modificare 
le variabili : h,min,sec ; e dei widget flutter per la creazione dei bottoni start,stop e reset,
che vanno a richiamare le funzioni omonime.
(la funzione reset volutamente se richiamata non ferma il cronometro ma lo azzera soltanto)

**Documentazione:** 
documentazione flutter ufficiale
stackoverflow

**UtimoCommit**
è stato effettuato un commit in seguito alla deadline poichè provando l'applicazione sul cellulare ho notato un errore,
dovuto ad un' operazione errata nella funzione stop, la quale toglieva un secondo quando veniva richiamata,per ovviare al problema di 
ritado dopo aver premoto il tasto.
