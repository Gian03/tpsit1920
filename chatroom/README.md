# SERVER:
il programma del server è stato preso dagli esempi del professore, e sono state apportate leggere modifiche.

# ChatRoom_Client:
nella cartella chatroom_client/lib sono presenti tre file:  main,chatpage,message;
Nel file main viene gestita la prima schermata cioè quella in cui si inserscono i parametri di ipAddress e nickname
,utilizzando un sistema di righe e colonne per posizionare i widget.Inoltre viene eseguita la funzione **connect()**,la quale
è richiamata quando viene premuto il tasto di login.Il metodo connect è un metodo Future < void > ,il quale con il Socket **writer**
si collega al server chatroom,successivamnte si aspetta che il socket si colleghi o meno con il codice : 
**await Socket.connect(InternetAddress(ipAddress), 3000)**,dove con await si aspetta che finisca Socket.connect;
se si è collegato si passa alla chat_page,altrimenti verrà visualizzato un alert di errore.
Nel file chat_page ,viene gestita la pagina di scambio dei messaggi, con una listView che viene riempita di **message** che è uno
stateless widget scritto nel file message.Alla pagina chat:page vengono passati **l'ipAddress** , il *nickname** , e il
**socket writer**.In questo file troviamo le funzioni:**listen()** la quale sta in ascolto del server e scrive i messagi ricevuti nella
listView; e close la quale disconnette il socket dal server e ci riporta alla pagina iniziale.