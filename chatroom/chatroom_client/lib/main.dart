import 'package:flutter/material.dart';
import 'chat_page.dart';
import 'dart:io';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Connection page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final nameController = new TextEditingController();
  final ipController = new TextEditingController();

  @override
  void dispose() {
    nameController.dispose();
    ipController.dispose();
    super.dispose();
  }

  String nickname = '';
  String ipAddress = '';
  Socket writer;
  bool isConnect = false;

  Future<void> connect() async {
    try {
      await Socket.connect(InternetAddress(ipAddress), 3000).then((client) {
        writer = client;
        isConnect = true;
      }).catchError((e) {
        print('error');
        if (e is SocketException) print('ScocketException => $e');
        setState(() {
          isConnect = false;
        });
      });
    } catch (e) {
      isConnect = false;
    }
  }

  Future <void> changePage() async {
    await connect();
    if (isConnect) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MyChatPage(ipAddress, nickname, writer)),
      );
    }else{
      showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text('ERROR',style: TextStyle(color: Colors.blue),),
            content: Text('Failed to connect:\nTry to change the ip address'),
          );
        }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Stack(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                      child: Text('Nickname: '),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      width: 200,
                      child: TextField(
                        controller: nameController,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
                      child: Text('IPaddress: '),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      width: 200,
                      child: TextField(
                        controller: ipController,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                      child: FloatingActionButton.extended(
                        onPressed: () async {
                          setState(() {
                            nickname = nameController.text;
                            ipAddress = ipController.text;
                          });
                          changePage();
                        },
                        icon: Icon(Icons.message),
                        label: Text('login'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
