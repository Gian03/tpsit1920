import 'package:flutter/material.dart';
import 'main.dart';
import 'message.dart';
import 'dart:io';

class MyChatPage extends StatelessWidget {

  String ipAddress;
  String nickname;
  Socket writer;

  MyChatPage(this.ipAddress,this.nickname,this.writer);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ChatPage(ipAddress,nickname,writer,title: 'Chatroom'),
    );
  }
}

class ChatPage extends StatefulWidget {
  ChatPage(this.ipAddress,this.nickname, this.writer,{Key key, this.title}) : super(key: key);
  Socket writer;
  bool isConnect = false;
  final String title;
  String ipAddress;
  String nickname;

  @override
  _ChatPage createState() => _ChatPage(ipAddress,nickname,writer);
}

class _ChatPage extends State<ChatPage> {
  ScrollController scrollController;
  bool isOnTop = true;

  @override
  void initState() {
    super.initState();
    listen(writer);
    scrollController = ScrollController();
  }

  _scrollToBottom() {
    scrollController.animateTo(0,
        duration: Duration(milliseconds: 10), curve: Curves.ease);
    setState(() {
      isOnTop = false;
    });
  }

  final myController = TextEditingController();

  @override
  void dispose() {
    myController.dispose();
    scrollController.dispose();
    super.dispose();
  }

  List messages = <String>[];
  List nicknames = <String>[];
  List originsMessages = <bool>[];

  //---------------------------------------------

  Socket writer;
  bool isConnect = false;
  String ipAddress;
  String myNickname;
  bool isFirstMessage=true;

  _ChatPage(this.ipAddress,this.myNickname,this.writer);

  void disconnect() {
    writer.close();
  }

  void listen(Socket server) {
    server.listen((data) {
      String s = String.fromCharCodes(data).trim();
      setState(() {
        if(isFirstMessage){
          nicknames.insert(0, 'Server');
          originsMessages.insert(0,true);
          messages.insert(0, 'Connesso');
          isFirstMessage=false;
        }else{
          String nickname='';
          String message='';
          bool p=false;
          int i=0;
          while(i<s.length){
            if(s[i]=='|'){
              p=true;
              i++;
            }else{
            if(!p){
              nickname+=s[i];
            }
            if(p){
              message+=s[i];
            }
            i++;
            }
          }              
          nicknames.insert(0, nickname);
          originsMessages.insert(0,true);
          messages.insert(0, message);
        }
      });
    },
    onDone: () {
      server.close();
    });
  }

  //---------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text('chat page'),
            Padding(
              padding: EdgeInsets.fromLTRB(140, 0, 10, 0),
            ),
            FlatButton.icon(
              onPressed: () {
                disconnect();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                );
              },
              icon: Icon(Icons.exit_to_app),
              label: Text('quit'),
              color: Colors.red,
            ),
          ],
        ),
      ),
//-----------------------------------------------------------
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) => const Divider(),
                controller: scrollController,
                reverse: true,
                padding: const EdgeInsets.all(10),
                itemCount: messages.length,
                itemBuilder: (BuildContext cotext, int index) {
                  return SizedBox(
                      width: 100, 
                      child: Message(messages[index], nicknames[index],originsMessages[index]));
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(left: 20),),
                Expanded(
                  child: TextField(
                    controller: myController,
                    onTap: () {
                      setState(() {
                        _scrollToBottom();
                      });
                    },
                  ),
                ),
                FlatButton(
                  padding: EdgeInsets.all(20),
                  onPressed: () {
                    setState(() {
                      nicknames.insert(0, myNickname);
                      originsMessages.insert(0,false);
                      messages.insert(0, myController.text);
                      FocusScope.of(context).requestFocus(new FocusNode());
                    });
                    writer.write(myNickname+'|'+myController.text);
                    myController.text = '';
                  },
                  child: Icon(
                    Icons.send,
                    color: Colors.purple,
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
            ),
          ],
        ),
      ),
    );
  }
}
