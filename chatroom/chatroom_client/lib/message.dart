import 'package:flutter/material.dart';

class Message extends StatelessWidget {
  
  String text;
  String nickname;
  bool isArrived=true;
  double _leftPadding;
  double _rightPadding;

  Message(this.text, this.nickname, this.isArrived);

  @override
  Widget build(BuildContext context) {
    if (isArrived) {
      _leftPadding = 0;
      _rightPadding = 180;
    } else {
      _leftPadding = 180;
      _rightPadding = 0;
    }
    return Container(
      padding: EdgeInsets.fromLTRB(_leftPadding, 5, _rightPadding, 5),
      width: 100,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                nickname,
                style: TextStyle(color: Colors.purple),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child : Container(
              child: Text(text),
            ),
          )
        ],
      ),
    );
  }
}
