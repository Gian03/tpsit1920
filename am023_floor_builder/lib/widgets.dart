import 'package:flutter/material.dart';

import 'task.dart';
import 'task_dao.dart';

class ListCell extends StatelessWidget {
  const ListCell({
    Key key,
    @required this.task,
    @required this.dao,
  }) : super(key: key);

  final Task task;
  final TaskDao dao;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: UniqueKey(),

      background: Container(
        color: Colors.red[900],
        alignment: Alignment.centerLeft,
        child: Icon(Icons.delete, color: Colors.white),
      ),

      secondaryBackground: Container(
        color: Colors.green,
        alignment: Alignment.centerRight,
        child: Icon(
          Icons.update,
          color: Colors.white,
        ),
      ),
      //direction: DismissDirection.endToStart,
      child: ListTile(title: Text(task.message)),
      onDismissed: (direction) async {
        print(direction);
        if (direction == DismissDirection.startToEnd) {
          await dao.deleteTask(task);
          Scaffold.of(context).showSnackBar(
            SnackBar(content: const Text('Removed task')),
          );
        } else {
          var textEditingController= TextEditingController();
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text("Alert Dialog title"),
                content: TextField(
                  controller: textEditingController,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Enter a search term'),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: new Text("Save"),
                    onPressed: () async {
                      var newTask = Task(task.id, textEditingController.text);
                      await dao.updateTask(newTask);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      },
    );
  }
}
