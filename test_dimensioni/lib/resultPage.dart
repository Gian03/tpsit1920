import 'package:flutter/material.dart';
import 'main.dart';

void main() => runApp(MyPage());

class MyPage extends StatelessWidget {
  MyPage({Key key, this.result, this.test});

  List<bool> test;
  String result;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Result Page',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(result, test, title: 'Result Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage(this.result, this.test, {Key key, this.title}) : super(key: key);

  String title;
  List<bool> test;
  String result;

  @override
  _MyResultPageState createState() => _MyResultPageState(result, test);
}

class _MyResultPageState extends State<MyHomePage> {
  _MyResultPageState(this.result, this.test);

  String result = "";
  List<bool> test = [];
  Color c;
  String str;

  void testGame() {
    if (test.contains(false)) {
      c = Colors.red;
      str = "WRONG";
    } else {
      c = Colors.green;
      str = "CORRECT";
    }
  }

  @override
  Widget build(BuildContext context) {
    testGame();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  result,
                  style: TextStyle(fontSize: 40),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  str,
                  style: TextStyle(fontSize: 40, color: c),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MyApp(),
                        ));
                  },
                  child: Text("restart game"),
                  color: c,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
