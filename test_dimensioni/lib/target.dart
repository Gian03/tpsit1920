import 'package:flutter/material.dart';

class MyTarget extends StatelessWidget {
  MyTarget({Key key, this.h, this.w, this.number, this.test,this.accept}) : super(key: key);

  final double h, w;
  final int number;
  final Function test;
  Widget c;
  bool accept;

  @override
  Widget build(BuildContext context) {
    return DragTarget(
      builder:
          (BuildContext context, List<int> candidateData, List rejectedData) {
        return Container(
          width: w,
          height: h,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.red, width: 3),
          ),
          child: c,
        );
      },
      onWillAccept: (data) {
        print("will");
        if(accept){
          return true;
        }else return false;
      },
      onAccept: (data) {
        print("accept");
        accept=false;
        String r = "$data" + "->" + "$number" + "  ";
        test(r, data == number);
        c = Container(
          color: Colors.red,
        );
      },
    );
  }
}