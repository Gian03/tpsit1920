import 'dart:math';

import 'package:flutter/material.dart';
import 'resultPage.dart';
import 'target.dart';
import 'draggable.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'test dimensioni'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<double> lList = [70, 100, 150];
  var draggableList = [];
  Random r = Random();
  List<DraggableSquare> dsList = [];
  String result = "";
  List<bool> resultList2 = [];

  void testGame(String r, bool t) {
    result = "$result" + "$r";
    resultList2.add(t);
    if (resultList2.length == 3) {
      print(result);
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MyPage(result: result, test: resultList2)),
      );
    }
  }

  void setL() {
    List<int> xList = [];
    for (int i = 0; xList.length < 3;) {
      var x = r.nextInt(3);
      if (!xList.contains(x)) {
        xList.add(x);
        print(xList[i]);
        draggableList.add(MyDraggable(l: lList[x], number: x));
        i++;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    setL();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  MyTarget(w: 70, h: 100, number: 0, test: testGame,accept: true),
                  MyTarget(w: 100, h: 150, number: 1, test: testGame,accept: true),
                  MyTarget(w: 150, h: 200, number: 2, test: testGame,accept: true),
                ]),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                draggableList[0],
                draggableList[1],
                draggableList[2],
              ],
            ),
          ],
        ),
      ),
    );
  }
}


