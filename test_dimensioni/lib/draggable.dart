import 'package:flutter/material.dart';

class MyDraggable extends StatefulWidget {
  MyDraggable({Key key, this.number,this.l}) : super(key: key);

  final int number;
  final double l;

  @override
  DraggableSquare createState() => DraggableSquare();
}

class DraggableSquare extends State<MyDraggable> {

  bool isDragged=false;

  @override
  Widget build(BuildContext context) {
    var data=widget.number;
    double l=widget.l;
    return Container(
      child: isDragged 
      ? Container(width: l,height: l)
      : Draggable(
        data: data,
        child: Container(
          width: l,
          height: l,
          color: Colors.red,
        ),
        childWhenDragging:Container(
          width: l,
          height: l,
        ),
        feedback: Container(
          width: l,
          height: l,
          color: Colors.blue,
        ),
        onDragCompleted: (){
          print("completato");
          setState(() {
            isDragged=true;
          });
        },
      ),
    );
  }
}