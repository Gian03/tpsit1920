# test_dimensioni

Da Medium invitiamo alla lettura di 
"A Deep Dive Into Draggable and DragTarget in Flutter" di D.Joshi [qui](https://medium.com/flutter-community/a-deep-dive-into-draggable-and-dragtarget-in-flutter-487919f6f1e4)

# main

Nel file main.dart vengono creati e posizionati i tre target e i tre draggable.
I draggable vengono creti a partire dal metodo `setL` , che va a settare il number di ogni draggable con un random fino a 3.
Invece i 3 target vengono creti semplicemente richiamando il widget MyTarget, passando i parametri di `w`: larghezza,`h` : altezza, `number` : il numero a cui corrisponde , `testGame` : il controllo(il quale se sono stati collocati tutti i draggable porta alla schermata del risultato) , e `accept` : bool che regola l'accettazione o meno di draggable.


# draggable

Nel file draggable.dart, si va a definire  il nostro draggable,il quale in base alla variabile `isDragged`, puo' essere o un container vuoto se questa è true,se false invece è un draggable.



# target

Nel file target viene definito il target, il quale accetta il draggable se il parametro accept è true,ed una volta accettato, va ad eseguire il metodo di test, e si riempie con un container colorato.

# resultPage

resultPage è la schermata che indica se si ha fatto il test giusto,riportando l'esito e le combinazioni effettuate,da qui è possibile ricominciare il gioco.



## draggable
`Draggable` [qui](https://api.flutter.dev/flutter/widgets/Draggable-class.html). La gestione del `Draggable` prevede
- `child` il *widget* da visualizzare quando nessun *drag*  attivo.
- `feedbach` è il *widget* che viene mostrato sotto il puntatore del mouse a drag attivo/i.
- `childWhenDragging` indica il *widget* mostrato, nel posto originale, quando il *drag* è attivo.
- `data`  è quanto viene passato tramite l'operazione di *drag and drop*.
-  `childWhenDragging` per la classe indica il massimo numero di istanza che possono avere il *drag*, ponendolo a zero si può inibire il *drag*.


