import 'package:covid19/api.dart';
import 'package:flutter/material.dart';
import 'singleCountryPage.dart';

class CardCountry extends StatelessWidget {
  Country w;

  CardCountry(this.w);

  Widget build(BuildContext context) {
    return Scaffold(
      body: FlatButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MySingleCountryPage(w)),
          );
        },
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.white,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(20),
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [
                  Color(0xFF3383CD),
                  Color(0xFF11249F),
                ]),
          ),
          width: 450,
          height: 200,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      width: 80,
                      height: 80,
                      decoration: new BoxDecoration(
                          color: Colors.black,
                          shape: BoxShape.circle,
                          border: new Border.all(
                            color: Colors.white,
                            width: 2.5,
                          ),
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: new NetworkImage(w.countryInfo.flag))),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text(w.country,style: TextStyle(color: Colors.white,fontSize: 14)),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Deaths: ${w.deaths}",style: TextStyle(color: Colors.red,fontSize: 15)),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Cases: ${w.cases}",style: TextStyle(color: Colors.orange,fontSize: 15),),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
