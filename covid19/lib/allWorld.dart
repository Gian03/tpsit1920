import 'package:flutter/material.dart';
import 'main.dart';
import 'api.dart';
import 'card.dart';
import 'custom_images_icons.dart';
import 'clipper.dart';

class MyAllWorldPage extends StatelessWidget {
  MyAllWorldPage(this.world, this.deaths, this.cases);

  List<Country> world;
  int deaths, cases;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Country",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePageState(world, deaths, cases),
    );
  }
}

class MyHomePageState extends StatefulWidget {
  MyHomePageState(this.world, this.deaths, this.cases);

  List<Country> world;
  int deaths, cases;

  @override
  AllWorldPage createState() => AllWorldPage(world, deaths, cases);
}

class AllWorldPage extends State<MyHomePageState> {
  AllWorldPage(this.world, this.deaths, this.cases);

  List<Country> world;
  int deaths, cases;

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          ClipPath(
            clipper: MyClipper(),
            child: Container(
              height: 315,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xFF3383CD),
                      Color(0xFF11249F),
                    ]),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(15),),
                  Align(
                    alignment: Alignment.topRight,
                    child: FlatButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MyApp()),
                        );
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                      label: Text(''),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Icon(
                      CustomImages.world,
                      size: 120,
                      color: Colors.white,
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Deaths: $deaths",
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Cases: $cases",
                      style: TextStyle(color: Colors.orange),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
              height: 580,
              child: ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: world.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        height: 200, child: CardCountry(world[index]));
                  })),
        ],
      ),
    );
  }
}
