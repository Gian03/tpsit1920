import 'package:flutter/material.dart';
import 'api.dart';
import 'main.dart';
import 'clipper.dart';
import 'singleCountry.dart';

void main() => runApp(MySingleCountryPage());

class MySingleCountryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Country",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePageState(),
    );
  }
}

class MyHomePageState extends StatefulWidget {
  @override
  MySearchCountryPage createState() => MySearchCountryPage();
}

class MySearchCountryPage extends State<MyHomePageState> {
  final myController = TextEditingController();
  Country c;
  Widget description;

  void dispose() {
    myController.dispose();
    super.dispose();
  }

  Future<Country> foudCountry() async {
    return await getSingleCountryByName(myController.text);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          ClipPath(
            clipper: MyClipper(),
            child: Container(
              height: 350,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xFF3383CD),
                      Color(0xFF11249F),
                    ]),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(20)),
                  Align(
                    alignment: Alignment.topRight,
                    child: FlatButton.icon(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MyApp()),
                        );
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                      label: Text(''),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(20)),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: 300,
                      child: TextField(
                        style: new TextStyle(color: Colors.white),
                        cursorColor: Colors.white,
                        controller: myController,
                        decoration: InputDecoration(
                          labelText: "Country",
                          labelStyle: TextStyle(color: Colors.white),
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(20)),
                  Align(
                    alignment: Alignment.center,
                    child: FlatButton(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      child: Icon(Icons.search),
                      onPressed: () async {
                        c = await foudCountry();
                        if (c != null) {
                          print("prova");
                          setState(() {
                            description = MyCountryCard(c);
                          });
                        } else {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Error"),
                                content: Text("Country not found"),
                                actions: <Widget>[
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text("exit"))
                                ],
                              );
                            },
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(height: 500, width: 400, child: description)
        ],
      ),
    );
  }
}
