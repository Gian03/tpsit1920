import 'package:flutter/material.dart';
import 'api.dart';
import 'singleCountry.dart';
import 'clipper.dart';

class MySingleCountryPage extends StatelessWidget {
  MySingleCountryPage(this.country);
  Country country;

  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          ClipPath(
            clipper: MyClipper(),
            child: Container(
              height: 250,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xFF3383CD),
                      Color(0xFF11249F),
                    ]),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(20)),
                  Align(
                    alignment: Alignment.topRight,
                    child: FlatButton.icon(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                      label: Text(''),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(20)),
                  Align(
                    alignment: Alignment.center,
                    child: Text(country.country,style: TextStyle(color: Colors.white,fontSize: 20),)                  )
                ],
              ),
            ),
          ),
          Padding(padding: EdgeInsets.all(15)),
          Container(
                  width: 400,
                  height: 500,
                  child: MyCountryCard(country),
                  ),
        ],
      ),
    );
  }
}
