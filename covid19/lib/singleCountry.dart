import 'package:flutter/material.dart';
import 'api.dart';

class MyCountryCard extends StatelessWidget{

  MyCountryCard(this.country);

  Country country;

  Widget build(BuildContext context){
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 150,
                  child: Image.network(country.countryInfo.flag),
                  ),
              ],
            ),
            Container(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(country.country,style: TextStyle(fontSize: 22)),
              ],
            ),
            Container(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Cases: ",style: TextStyle(fontSize: 20)),
                Text("${country.cases}",style: TextStyle(color: Colors.orange,fontSize: 20),)
              ],
            ),
            Container(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Today cases: ",style: TextStyle(fontSize: 20)),
                Text("${country.todayCases}",style: TextStyle(color: Colors.orange,fontSize: 20))
              ],
            ),
            Container(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Deaths: ",style: TextStyle(fontSize: 20)),
                Text("${country.deaths}",style: TextStyle(color: Colors.red,fontSize: 20)),
              ],
            ),
            Container(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Today deaths: ",style: TextStyle(fontSize: 20)),
                Text("${country.todayDeaths}",style: TextStyle(color: Colors.red,fontSize: 20))
              ],
            ),
            Container(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Recovered: ",style: TextStyle(fontSize: 20)),
                Text("${country.recovered}",style: TextStyle(color: Colors.green,fontSize: 20))
              ],
            ),
            Container(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Active: ",style: TextStyle(fontSize: 20)),
                Text("${country.active}",style: TextStyle(color: Colors.orange,fontSize: 20))
              ],
            ),
            Container(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Critical: ",style: TextStyle(fontSize: 20)),
                Text("${country.critical}",style: TextStyle(color: Colors.red,fontSize: 20))
              ],
            ),
            Container(height: 5),                  
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Tests: ",style: TextStyle(fontSize: 20)),
                Text("${country.tests}",style: TextStyle(color: Colors.cyan,fontSize: 20))
              ],
            ),
          ],
        ),
      ),
    );
    }
  
}