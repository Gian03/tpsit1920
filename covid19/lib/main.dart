import 'package:covid19/allWorld.dart';
import 'package:covid19/api.dart';
import 'package:flutter/material.dart';
import 'searchSingleCountryPage.dart';
import 'clipper.dart';
import 'custom_images_icons.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  List<Country> world;
  int deaths,cases;


  Future<int> getWorldDeaths() async{
    var t=0;
    for(int i=0;i<world.length;i++){
      t+=world[i].deaths;
    }
    return t;
  }

  Future<int> getWorldCases() async{
    var t=0;
    for(int i=0;i<world.length;i++){
      t+=world[i].cases;
    }
    return t;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          ClipPath(
            clipper: MyClipper(),
            child: Container(
              height: 450,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xFF3383CD),
                      Color(0xFF11249F),
                    ]),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(30),),
                  Container(
                    height: 250,
                    child: Align(
                      alignment: Alignment.center,
                      child: Icon(CustomImages.bacteria,size: 250,color: Colors.white,),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(padding: EdgeInsets.all(50),),
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.search,color: Colors.white,),
                  color: Color(0xFF3383CD),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MySingleCountryPage()),
                    );
                  },
                  label: Text("Search single country",style: TextStyle(color: Colors.white),),
                )
              ],
            ),
            Padding(padding: EdgeInsets.all(30),),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.search,color: Colors.white,),
                  onPressed: () async {
                    world= await getAllWorld();
                    deaths=await getWorldDeaths();
                    cases=await getWorldCases();
                    if(world != null){
                      Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MyAllWorldPage(world,deaths,cases)),
                    );
                    }
                  },
                  label: Text("All world",style: TextStyle(color: Colors.white)),
                  color: Color(0xFF11249F),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),),
                )
              ],
            ),
        ],
      ),
    );
  }
}
