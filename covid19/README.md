# covid19

A new Flutter project.


# HomePage

Appena si apre l'app, troviamo un logo raffigurante il virus,ottenuto scaricando da internet il logo e crenado il file `custom_images_icon.dart`, il quale contiene la classe `CustomImage`,inoltre per ottenere le nuove icone bisogna modificare il file pubspec.yaml aggiungendo:

fonts:
  - family:  CustomImages
    fonts:
      - asset: fonts/CustomImages.ttf

Fatto questo basterà richimare la classe CustomImage.bacteria.
Sotto troviamo due tasti,il primo `Search single country`, ed il secondo `allworld`.

# Search single country

Quando viene premuto, con un navigator si viene rimandati alla page `searchSingleCountryPage`, nella quale è presente un `TextField`, per l'immissione del paese da cercare.
Successivamente per completare la ricerca bisogna premete il tasto sottostate; il quale va a settare il parametro `c` richiamando il metodo `foundCountry`, il quale ritorna il  `Future<Country>` da noi richiesto, e se l'operazione non va a buon fine, cioè se si riceve un messaggio 400, viene mostrato un alert.Una volta ottenuto il country, nel resto della pagina viene creato il widget `MyCountryCard`, il quale riporta tutti a diti di questo.

# allWorld

Quando viene premuto questo tasto invece, dalla homePage viene richiamato il metodo `getAllWorld`, contenuto nel file api; il quale ritorna un `Future<List<Country>>` , una lista contenente tutti i country strovati nelle api.
Inoltre vengono chiamati i metodi `getWorldDeaths` e `getWorldCases`, i quali ritornano un `Future<int>`, rappresentante i numeri di morti e di contagiati totali.
Successivamente si viene portatati alla `AllWorldPage` ,la quale riporta in alto i dati passati dalla home page che sono morti e contagiati totali.
Sotto troviamo una `ListView` di `CardCountry`, le quali riportano la bandiera,i contagiati e i morti di quel Country;questa card sono inrealtà dei FlatButton, che se premuti ci mandano alla `MysingleCountryPage`, una pagina che riporta la 
`MyCountryCard` del Country selezionato.


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
