import 'package:flutter/material.dart';
import 'main.dart';

class Q extends CustomPainter {
  @override
  Q(this.q,this.pos);
  var q,pos;
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10;
    if (q) {
      if (tabG[pos] == 1)
        paint.color = Colors.red;
      else
        paint.color = Colors.green;
      canvas.drawCircle(Offset(10, 10), 35, paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}