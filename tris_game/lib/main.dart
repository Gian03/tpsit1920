import 'package:flutter/material.dart';
import 'quadrato.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tris',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Tris'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  MyAppTris createState() => MyAppTris();
}

var tabG = List(9)..fillRange(0, 9, 0);
var giocatore = 1;
var tap =0;

class MyAppTris extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: Text("Tris Game"),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  runApp(MyApp());
                  tabG.fillRange(0,9, 0);
                  giocatore=1;
                  tap=0;
                },
                child: Text("restart game"),
              ),
            ],
          ),
          body: Center(
            child: Stack(
              children: <Widget>[
                CustomPaint(
                  painter: MyPainter(),
                  child: Center(
                    child: Stack(
                      children: <Widget>[
//-----------------------------------------------------------Quadrato1
                        Quadrato(false,270,60,220,15,0),
//-----------------------------------------------------------Quadrato2
                        Quadrato(false,270,190,220,150,1),
//-----------------------------------------------------------Quadrato3
                        Quadrato(false,270,320,220,287,2),
//-----------------------------------------------------------Quadrato4
                        Quadrato(false,400,60,360,15,3),
//-----------------------------------------------------------Quadrato5
                        Quadrato(false,400,190,360,150,4),
//-----------------------------------------------------------Quadrato6
                        Quadrato(false,400,320,360,287,5),
//-----------------------------------------------------------Quadrato7
                        Quadrato(false,530,60,495,15,6),
//-----------------------------------------------------------Quadrato8
                        Quadrato(false,530,190,500,150,7),
//-----------------------------------------------------------Quadrato9
                        Quadrato(false,530,320,500,287,8),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 2;
    canvas.drawLine(Offset(135, size.width / 2),
        Offset(135, size.height - size.width / 2), paint); //verticale destra
    canvas.drawLine(
        Offset(size.width - 135, size.width / 2),
        Offset(size.width - 135, size.height - size.width / 2),
        paint); //verticale sinistra
    canvas.drawLine(
        Offset(10, size.width / 2 + (size.width / 3)),
        Offset(size.width - 10, size.width / 2 + (size.width / 3)),
        paint); //orizzontale sopra
    canvas.drawLine(
        Offset(10, size.width / 2 + 2 * (size.width / 3)),
        Offset(size.width - 10, size.width / 2 + 2 * (size.width / 3)),
        paint); //orizzontale sotto
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
