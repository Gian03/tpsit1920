import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'customPainer.dart';
import 'main.dart';

class Quadrato extends StatefulWidget {
  Quadrato(this.q, this.t1, this.l1, this.t2, this.l2, this.pos);
  var q, pos;
  double t1, l2, t2, l1;

  State<Quadrato> createState() {
    return QuadratoState();
  }
}

class QuadratoState extends State<Quadrato> {
  
  Future alertWidget(var i){
    var w;
    if(i==-1) w='pareggio';
    else if(tabG[i]==1) w = 'rosso';
    else w = 'verde';
    
    return showDialog(
        context: context,
        barrierDismissible: false, 
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Il vincitore è il giocatore: '),
            content: Container(
              child: Container(
                width: 100,
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[Text('$w')],
                )
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: (){
                  runApp(MyApp());
                  tabG.fillRange(0,9, 0);
                  giocatore=1;
                  tap=0;
                  Navigator.pop(context);
                }, 
                child: Text('restart game'),color: Colors.red,)
            ],
          );
        }
      );
  }
  
  Future controllo() {
    
    if (tabG[0]!=0 && tabG[0]==tabG[1] && tabG[1] == tabG[2]) {
      return alertWidget(0);
    }else if( tabG[3]!=0 && tabG[3]==tabG[4] && tabG[4] == tabG[5]){
      return alertWidget(3);
    }else if(tabG[6]!=0 && tabG[6]==tabG[7] && tabG[7] == tabG[8]){
      return alertWidget(6);
    }else if(tabG[0]!=0 && tabG[0]==tabG[3] && tabG[3] == tabG[6]){
      return alertWidget(0);
    }else if(tabG[1]!=0 && tabG[1]==tabG[4] && tabG[4] == tabG[7]){
      return alertWidget(1);
    }else if(tabG[2]!=0 && tabG[2]==tabG[5] && tabG[5] == tabG[8]){
      return alertWidget(2);
    }else if(tabG[0]!=0 && tabG[0]==tabG[4] && tabG[4] == tabG[8]){
      return alertWidget(0);
    }else if(tabG[2]!=0 && tabG[2]==tabG[4] && tabG[4] == tabG[6]){
      return alertWidget(2);
    }else if (tap==9){
      return alertWidget(-1);
    }
    return null;
  }

  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
          top: widget.t1,
          left: widget.l1,
          child: CustomPaint(
            painter: new Q(widget.q, widget.pos),
          ),
        ),
        Positioned(
          top: widget.t2,
          left: widget.l2,
          child: ButtonTheme(
            minWidth: 110.0,
            height: 110.0,
            child: FlatButton(
                onPressed: () {
                  if (tabG[widget.pos] == 0) {
                    tap++;
                    widget.q = true;
                    setState(() {
                      tabG[widget.pos] = giocatore;
                      controllo();
                      giocatore = giocatore % 2 + 1;
                    });
                  }
                },
                child: Text("")),
          ),
        ),
      ],
    );
  }
}
