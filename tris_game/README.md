# tris_game

A new Flutter project.

# main

nel file main, viene disegnata la griglia di gioco di un CustomPainter,e vengono posizionati i widget `quadrato` all'interno della griglia , ai quali vengono passati i parametri per il posizionamento, e false,cioè un bool che indica che non è stato ancora occupato.

# quadrato

nel file quadrato, si definisce il widget `quadrato`.
Come prima cosa,utilizzando i parametri passati dalla home page,vengono creati come FlatButton che se schiacciati, vanno a passare al `CustomPaint` il parametro bool true, il quale andrà a posizionare un cerchio rosso o verde in base al giocatore;inoltre si cambia giocatore, e si va ad eseguire il test per vedere se in quella mossa un giocatore ha vinto, o si hanno finito le celle e quindi c'è un pareggio. 

# customPainter
qui si va a definire il customPainer che viene richiamato dal widget quadrato,che va semplicemente a disegnare un cerchio rosso o verde all'interno della cella premuta.


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
